locals {
  state_aws_region = "ca-central-1"
}

remote_state {
  backend = "s3"
  generate = {
    path      = "backend.tf"
    if_exists = "overwrite_terragrunt"
  }
  config = {
    bucket = "terraform-state-lt"
    /* bucket = "terraform-state-lt-02" */

    key            = "${path_relative_to_include()}/terraform.tfstate"
    region         = local.state_aws_region
    encrypt        = true
    dynamodb_table = "terraform-lock-table"

    shared_credentials_file = "/Users/airatkh/.aws/credentials"
    /* profile = "production" */
    profile = "default"
    
  }
}

terraform {
  extra_arguments "common_vars" {
    commands = get_terraform_commands_that_need_vars()

    # optional_var_files = [
    #   find_in_parent_folders("regional.tfvars"),
    # ]

    optional_var_files = [
      "${get_terragrunt_dir()}/../regional.tfvars"
    ]

  }

}


generate "provider" {
  path      = "provider.tf"
  if_exists = "overwrite_terragrunt"
  contents  = <<EOF
provider "aws" {
  region  = var.aws_region
  #allowed_account_ids = ["717774038078"]
  allowed_account_ids = ["776347173081"]
  

  shared_credentials_file = "/Users/airatkh/.aws/credentials"
  #profile = "production"
  profile = "default"
}

variable "aws_region" {
  description = "AWS region to create infrastructure in"
  type        = string
}

/* variable "aws_region_secondary" {
  description = "AWS Secondary region to create infrastructure in"
  type        = string
} */
EOF
}

