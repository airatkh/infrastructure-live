

# Template terraform terragrunt.

## [Operating a multi-regional application using Terraform Tool](docs/production_infrastructure_engineering/production_infrastructure_engineering.md)

## `Folders structure`

```
➜  infrastructure-live git:(master) ✗ tree . -L 4
.
├── README.md
├── credentials_env.hcl
├── dev
│   ├── ca-central-1
│   │   ├── aws-data
│   │   │   └── terragrunt.hcl
│   │   ├── eks
│   │   │   └── terragrunt.hcl
│   │   ├── rds-aurora
│   │   │   └── terragrunt.hcl
│   │   ├── regional.hcl
│   │   ├── regional.tfvars
│   │   └── vpc
│   │       └── terragrunt.hcl
│   ├── terragrunt.hcl
│   └── us-west-1
│       ├── aws-data
│       │   └── terragrunt.hcl
│       ├── eks
│       │   └── terragrunt.hcl
│       ├── rds-aurora-secondary
│       │   └── terragrunt.hcl
│       ├── regional.hcl
│       ├── regional.tfvars
│       └── vpc
│           └── terragrunt.hcl
├── prod
│   ├── ca-central-1
│   │   ├── aws-data
│   │   │   ├── terragrunt-debug.tfvars.json
│   │   │   └── terragrunt.hcl
│   │   ├── eks
│   │   │   ├── terragrunt-debug.tfvars.json
│   │   │   └── terragrunt.hcl
│   │   ├── regional.hcl
│   │   ├── regional.tfvars
│   │   └── vpc
│   │       └── existing_vpc_data.hcl
│   ├── terragrunt.hcl
│   └── us-west-1
├── stage
│   ├── ca-central-1
│   ├── terragrunt.hcl
│   └── us-west-1
├── modules
│   ├── aws-data
│   │   ├── README.md
│   │   ├── main.tf
│   │   └── outputs.tf
│   ├── eks-irsa
│   │   ├── README.md
│   │   ├── irsa.tf
│   │   ├── main.tf
│   │   ├── outputs.tf
│   │   ├── variables.tf
│   │   └── versions.tf
│   ├── rds-global-cluster-primary
│   │   ├── main.tf
│   │   ├── outputs.tf
│   │   ├── variables.tf
│   │   └── versions.tf
│   └── rds-global-cluster-secondary
│       ├── main.tf
│       ├── outputs.tf
│       ├── variables.tf
│       └── versions.tf
└── terragrunt_root.hcl

```

## Versions

https://terragrunt.gruntwork.io/

```
terragrunt 
VERSION:
   v0.35.13
```

https://www.terraform.io/

```
➜  eks git:(master) terragrunt version
Terraform code may not work. 
Terraform v0.14.9
```
