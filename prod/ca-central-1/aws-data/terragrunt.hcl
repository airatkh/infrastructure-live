terraform {
  source = "${get_parent_terragrunt_dir()}/modules/aws-data"
}

include "root" {
  path = find_in_parent_folders("terragrunt_root.hcl")
}


locals {
  regional = read_terragrunt_config(find_in_parent_folders("regional.hcl"))
}

inputs = {
  region = local.regional.inputs.aws_region
}