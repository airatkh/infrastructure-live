terraform {
  source = "git::git@github.com:terraform-aws-modules/terraform-aws-vpc.git?ref=v3.11.0"
}


include "root" {
    path = find_in_parent_folders("terragrunt_root.hcl")
}

locals {
  regional = read_terragrunt_config(find_in_parent_folders("regional.hcl"))
}


dependencies {
  paths = ["../aws-data"]
}

dependency "aws-data" {
  config_path = "../aws-data"
}

inputs = {
  aws_region = local.regional.inputs.aws_region
  eks_cluster_name = local.regional.inputs.eks_cluster_name

  name = format("vpc-main-%s", dependency.aws-data.outputs.aws_region.name)
  cidr = "10.0.0.0/16"

  azs = [for v in dependency.aws-data.outputs.available_aws_availability_zones_names : v]
  private_subnets = ["10.0.1.0/24", "10.0.2.0/24", "10.0.3.0/24"]
  public_subnets  = ["10.0.101.0/24", "10.0.102.0/24", "10.0.103.0/24"]
  database_subnets = ["10.0.201.0/24", "10.0.202.0/24", "10.0.203.0/24"]

  enable_nat_gateway   = true
  single_nat_gateway   = true
  enable_dns_hostnames = true
  
  public_subnet_tags = {
    "kubernetes.io/cluster/${local.regional.inputs.eks_cluster_name}" = "shared"
    "kubernetes.io/role/elb"              = "1"
  }

  private_subnet_tags = {
    "kubernetes.io/cluster/${local.regional.inputs.eks_cluster_name}" = "shared"
    "kubernetes.io/role/internal-elb"     = "1"
  }

  database_subnet_tags = {
    "kubernetes.io/cluster/${local.regional.inputs.eks_cluster_name}" = "shared"
    "kubernetes.io/role/internal-elb"     = "1"
  }

  tags = {
    Terraform = "true"
    Environment = "dev"
  }
}