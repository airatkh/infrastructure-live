terraform {
  source = "${get_parent_terragrunt_dir()}/modules/rds-global-cluster-secondary"
}

include "root" {
  path = find_in_parent_folders("terragrunt_root.hcl")
}

locals {
  regional = read_terragrunt_config(find_in_parent_folders("regional.hcl"))
  credentials = read_terragrunt_config(find_in_parent_folders("credentials_env.hcl"))
}

dependency "aws-data" {
  config_path = "../aws-data"
}

# dependencies VPC US
dependency "vpc-us" {
  config_path = "../vpc"
}

# dependencies VPC US
dependency "rds-aurora-primary" {
  config_path = "../../ca-central-1/rds-aurora"
}

inputs = {
  # aws_region = local.regional.inputs.aws_region
  # eks_cluster_name = local.regional.inputs.eks_cluster_name

  # Credentials Data.
  # credentials = local.credentials.inputs

  region = local.regional.inputs.aws_region
  global_cluster_identifier_name = "rds-cluster-test-name"

  vpc_id = dependency.vpc-us.outputs.vpc_id
  database_subnet_group_name = dependency.vpc-us.outputs.database_subnet_group_name
  private_subnets_cidr_blocks = dependency.vpc-us.outputs.private_subnets_cidr_blocks

  source_region = local.regional.inputs.aws_primary_region
  is_primary_cluster = false

  aws_rds_global_cluster_this_engine = dependency.rds-aurora-primary.outputs.aws_rds_global_cluster_this_engine
  aws_rds_global_cluster_this_engine_version = dependency.rds-aurora-primary.outputs.aws_rds_global_cluster_this_engine_version
  aws_rds_global_cluster_this_id = dependency.rds-aurora-primary.outputs.aws_rds_global_cluster_this_id

  aurora_tags = {
    Environment = "dev"
    Terraform = "true"
    App = "rds-aurora"
    Region = local.regional.inputs.aws_region
  }
}