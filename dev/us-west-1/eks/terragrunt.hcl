terraform {
  source = "../../../modules/eks-irsa"
}

include "root" {
  path = find_in_parent_folders("terragrunt_root.hcl")
}

locals {
  regional = read_terragrunt_config(find_in_parent_folders("regional.hcl"))
}


# dependencies Aws-Data
dependencies {
  paths = ["../aws-data"]
}

dependency "aws-data" {
  config_path = "../aws-data"
}

# dependencies VPC
dependency "vpc" {
  config_path = "../vpc"
}


# EKS input vars
inputs = {
  aws_region = local.regional.inputs.aws_region
  eks_cluster_name = local.regional.inputs.eks_cluster_name
  cluster_version = local.regional.inputs.cluster_version

  region = dependency.aws-data.outputs.aws_region.name
  cluster_name = format("lt-%s", dependency.aws-data.outputs.aws_region.name)

  vpc_id = dependency.vpc.outputs.vpc_id
  private_subnets = dependency.vpc.outputs.private_subnets

  tags = {
    Terraform = "true"
    Environment = "dev"
  }

  ################################################################################
  # Additional security groups for workers
  ################################################################################

  # resource "aws_security_group" "worker_group_mgmt_one" {
  #   name_prefix = "worker_group_mgmt_one"
  #   vpc_id      = dependency.vpc.outputs.vpc_id

  #   ingress {
  #     from_port = 22
  #     to_port   = 22
  #     protocol  = "tcp"

  #     cidr_blocks = [
  #       "10.0.0.0/8",
  #     ]
  #   }
  # }

  # resource "aws_security_group" "worker_group_mgmt_two" {
  #   name_prefix = "worker_group_mgmt_two"
  #   vpc_id      = dependency.vpc.outputs.vpc_id

  #   ingress {
  #     from_port = 22
  #     to_port   = 22
  #     protocol  = "tcp"

  #     cidr_blocks = [
  #       "192.168.0.0/16",
  #     ]
  #   }
  # }

  # resource "aws_security_group" "all_worker_mgmt" {
  #   name_prefix = "all_worker_management"
  #   vpc_id      = dependency.vpc.outputs.vpc_id

  #   ingress {
  #     from_port = 22
  #     to_port   = 22
  #     protocol  = "tcp"

  #     cidr_blocks = [
  #       "10.0.0.0/8",
  #       "172.16.0.0/12",
  #       "192.168.0.0/16",
  #     ]
  #   }
  # }

}