inputs = {
    aws_region = "us-west-1"
    aws_primary_region = "ca-central-1"
    eks_cluster_name = "lt-dev-us-west1"
    cluster_version = "1.21.2"
}