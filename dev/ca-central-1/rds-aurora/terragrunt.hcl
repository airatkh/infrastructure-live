terraform {
  source = "${get_parent_terragrunt_dir()}/modules/rds-global-cluster-primary"
}

include "root" {
  path = find_in_parent_folders("terragrunt_root.hcl")
}

locals {
  regional = read_terragrunt_config(find_in_parent_folders("regional.hcl"))
  credentials = read_terragrunt_config(find_in_parent_folders("credentials_env.hcl"))
}

dependency "aws-data" {
  config_path = "../aws-data"
}

# dependencies VPC Canada
dependency "vpc-ca" {
  config_path = "../vpc"
}


inputs = {
  # aws_region = local.regional.inputs.aws_region
  # eks_cluster_name = local.regional.inputs.eks_cluster_name

  # Credentials Data.
  # credentials = local.credentials.inputs

  region = local.regional.inputs.aws_region
  # region_secondary = local.regional.inputs.aws_region_secondary

  global_cluster_identifier_name = "rds-cluster-test-name"

  vpc_id = dependency.vpc-ca.outputs.vpc_id
  database_subnet_group_name = dependency.vpc-ca.outputs.database_subnet_group_name
  private_subnets_cidr_blocks = dependency.vpc-ca.outputs.private_subnets_cidr_blocks


  aurora_tags = {
    Environment = "dev"
    Terraform = "true"
    App = "rds-aurora"
    Region = local.regional.inputs.aws_region
  }
}