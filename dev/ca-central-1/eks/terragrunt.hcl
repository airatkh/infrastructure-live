terraform {
  source = "../../../modules/eks-irsa"
}

include "root" {
  path = find_in_parent_folders("terragrunt_root.hcl")
}

locals {
  regional = read_terragrunt_config(find_in_parent_folders("regional.hcl"))
}


# dependencies Aws-Data
dependencies {
  paths = ["../aws-data"]
}

dependency "aws-data" {
  config_path = "../aws-data"
}

# dependencies VPC
dependency "vpc" {
  config_path = "../vpc"
}


# EKS input vars
inputs = {
  aws_region = local.regional.inputs.aws_region
  cluster_name = local.regional.inputs.eks_cluster_name
  cluster_version = local.regional.inputs.cluster_version

  region = dependency.aws-data.outputs.aws_region.name

  # Original: vpc-0bfbad0a1a3492404
  # vpc_id = "vpc-0a14d532150e1c7a1"
  vpc_id = dependency.vpc.outputs.vpc_id


  # Original: 
  # main-vpc-private-ca-central-1a => subnet-0b8eb1486796ab67e
  # main-vpc-private-ca-central-1b => subnet-0369c40008a48d68d
  # main-vpc-private-ca-central-1d => subnet-0a85a4a47d74aea64
  # private_subnets = [
  #   "subnet-0b8eb1486796ab67e",
  #   "subnet-0369c40008a48d68d",
  #   "subnet-0a85a4a47d74aea64",
  # ]

  # private_subnets = [
  #   "subnet-0915b38af7c5af83b",
  #   "subnet-0fffddbbbdc4ee738",
  #   "subnet-0398166da5c12f502",
  # ]
  private_subnets = dependency.vpc.outputs.private_subnets

  tags = {
    Terraform = "true"
    Environment = "prod"
  }

  ################################################################################
  # Additional security groups for workers
  ################################################################################

  # resource "aws_security_group" "worker_group_mgmt_one" {
  #   name_prefix = "worker_group_mgmt_one"
  #   vpc_id      = dependency.vpc.outputs.vpc_id

  #   ingress {
  #     from_port = 22
  #     to_port   = 22
  #     protocol  = "tcp"

  #     cidr_blocks = [
  #       "10.0.0.0/8",
  #     ]
  #   }
  # }

  # resource "aws_security_group" "worker_group_mgmt_two" {
  #   name_prefix = "worker_group_mgmt_two"
  #   vpc_id      = dependency.vpc.outputs.vpc_id

  #   ingress {
  #     from_port = 22
  #     to_port   = 22
  #     protocol  = "tcp"

  #     cidr_blocks = [
  #       "192.168.0.0/16",
  #     ]
  #   }
  # }

  # resource "aws_security_group" "all_worker_mgmt" {
  #   name_prefix = "all_worker_management"
  #   vpc_id      = dependency.vpc.outputs.vpc_id

  #   ingress {
  #     from_port = 22
  #     to_port   = 22
  #     protocol  = "tcp"

  #     cidr_blocks = [
  #       "10.0.0.0/8",
  #       "172.16.0.0/12",
  #       "192.168.0.0/16",
  #     ]
  #   }
  # }

}