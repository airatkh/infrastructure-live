data "aws_caller_identity" "current" {}

data "aws_iam_policy_document" "rds" {
  statement {
    sid       = "Enable IAM User Permissions"
    actions   = ["kms:*"]
    resources = ["*"]

    principals {
      type = "AWS"
      identifiers = [
        "arn:aws:iam::${data.aws_caller_identity.current.account_id}:root",
        data.aws_caller_identity.current.arn,
      ]
    }
  }

  statement {
    sid = "Allow use of the key"
    actions = [
      "kms:Encrypt",
      "kms:Decrypt",
      "kms:ReEncrypt*",
      "kms:GenerateDataKey*",
      "kms:DescribeKey"
    ]
    resources = ["*"]

    principals {
      type = "Service"
      identifiers = [
        "monitoring.rds.amazonaws.com",
        "rds.amazonaws.com",
      ]
    }
  }
}

resource "aws_kms_key" "this" {
  policy = data.aws_iam_policy_document.rds.json
  tags   = var.aurora_tags
}

resource "aws_rds_cluster_parameter_group" "this" {
  name        = "aurora-mysql5-7-stage-dev"
  family      = "aurora-mysql5.7"
  description = "RDS default cluster parameter group Dev & Stage"

  parameter {
    name  = "character_set_client"
    value = "utf8mb4"
  }

  parameter {
    name  = "character_set_connection"
    value = "utf8mb4"
  }

  parameter {
    name  = "character_set_database"
    value = "utf8mb4"
  }

  parameter {
    name  = "character_set_filesystem"
    value = "binary"
  }

  parameter {
    name  = "character_set_results"
    value = "utf8mb4"
  }

  parameter {
    name  = "character_set_server"
    value = "utf8mb4"
  }

  parameter {
    name  = "collation_connection"
    value = "utf8mb4_unicode_ci"
  }
  
  parameter {
    name  = "collation_server"
    value = "utf8mb4_unicode_ci"
  }

  tags_all = var.aurora_tags
}

################################################################################
# RDS Aurora Module
################################################################################

module "aurora" {
  source  = "terraform-aws-modules/rds-aurora/aws"
  version = "6.1.3"

  is_primary_cluster        = var.is_primary_cluster
  source_region             = var.source_region

  name                      = var.global_cluster_identifier_name
  /* database_name             = var.aws_rds_global_cluster.this.database_name */
  engine                    = var.aws_rds_global_cluster_this_engine
  engine_version            = var.aws_rds_global_cluster_this_engine_version
  global_cluster_identifier = var.aws_rds_global_cluster_this_id
  instance_class            = "db.r5.large"
  instances                 = { for i in range(2) : i => {} }
  kms_key_id                = aws_kms_key.this.arn

  vpc_id                 = var.vpc_id
  db_subnet_group_name   = var.database_subnet_group_name
  create_db_subnet_group = false
  create_security_group  = true
  allowed_cidr_blocks    = var.private_subnets_cidr_blocks

  skip_final_snapshot = true
  apply_immediately = true

  /*
    Description: Instance parameter group to associate with all instances of the DB cluster. 
    The `db_cluster_db_instance_parameter_group_name` is only valid in combination with `allow_major_version_upgrade`
  */
  db_cluster_parameter_group_name = aws_rds_cluster_parameter_group.this.name

  tags = var.aurora_tags
}