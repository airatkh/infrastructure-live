variable "vpc_id" {
  description = "Controls if VPC should be created (it affects almost all resources)"
  type        = string
}

variable "cluster_version" {
  description = "Name to be used on all the resources as identifier"
  type        = string
}

variable "cluster_name" {
  description = "Cluster name"
  type        = string
}

variable "region" {
  description = "Cluster Region"
  type        = string
}

variable "private_subnets" {
    description = "Private_Subnets"
    type = list(string)
}