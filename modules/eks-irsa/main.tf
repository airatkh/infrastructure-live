################################################################################
# EKS Module
################################################################################

module "eks" {
  source  = "terraform-aws-modules/eks/aws"
  version = "17.24.0"

  cluster_version = var.cluster_version
  cluster_name  = var.cluster_name
  vpc_id  = var.vpc_id
  subnets = var.private_subnets

  /* worker_ami_owner_id = "961992271922"
  worker_ami_owner_id_windows = "amazon" */

  cluster_endpoint_private_access = true
  cluster_endpoint_public_access  = true
  enable_irsa = true
  manage_aws_auth  = false

  node_groups = {
    node_groups_01 = {
      desired_capacity = 1
      max_capacity     = 10
      min_capacity     = 1

      disk_size = 50

      /* instance_types = ["t3.medium"] */
      instance_types = ["c6i.2xlarge"]
      
      capacity_type = "ON_DEMAND"
      k8s_labels = {
        Environment = "prod"
        GithubRepo  = "terraform-aws-eks"
        GithubOrg   = "terraform-aws-modules"
      }
      additional_tags = {
          "key"                 = "k8s.io/cluster-autoscaler/enabled"
          "propagate_at_launch" = "false"
          "value"               = "true"
      }
      tags = {
          "key"                 = "k8s.io/cluster-autoscaler/${var.cluster_name}"
          "propagate_at_launch" = "false"
          "value"               = "owned"
      }
      
      update_config = {
        max_unavailable_percentage = 50 # or set `max_unavailable`
      }
    }
  }

  # AWS Auth (kubernetes_config_map)
  /* map_roles = [
    {
      rolearn  = "arn:aws:iam::66666666666:role/role1"
      username = "role1"
      groups   = ["system:masters"]
    },
  ] */

  map_users = [
      {
        userarn  = "arn:aws:iam::${data.aws_caller_identity.current.account_id}:user/akozhakhmetov"
        username = "akozhakhmetov"
        groups   = ["system:masters"]
      },
      {
        userarn  = "arn:aws:iam::${data.aws_caller_identity.current.account_id}:user/akhisamov"
        username = "akhisamov"
        groups   = ["system:masters"]
      },
      {
        userarn  = "arn:aws:iam::${data.aws_caller_identity.current.account_id}:user/ci-user"
        username = "ci-user"
        groups   = ["system:masters"]
      },
      {
        userarn  = "arn:aws:iam::${data.aws_caller_identity.current.account_id}:user/adaniyelian"
        username = "adaniyelian"
        groups   = ["system:masters"]
      },
      {
        userarn  = "arn:aws:iam::${data.aws_caller_identity.current.account_id}:user/amanvirdy"
        username = "amanvirdy"
        groups   = ["system:users"]
      },
      {
        userarn  = "arn:aws:iam::${data.aws_caller_identity.current.account_id}:user/daksh"
        username = "daksh"
        groups   = ["system:users"]
      },
      {
        userarn  = "arn:aws:iam::${data.aws_caller_identity.current.account_id}:user/rbryant17"
        username = "rbryant17"
        groups   = ["system:masters"]
      },
      {
        userarn  = "arn:aws:iam::${data.aws_caller_identity.current.account_id}:user/shifdra"
        username = "shifdra"
        groups   = ["system:masters"]
      },
      {
        userarn  = "arn:aws:iam::${data.aws_caller_identity.current.account_id}:user/CloudWatchAgent"
        username = "CloudWatchAgent"
        groups   = ["system:masters"]
      },
    ]


  tags = {
    Example    =  var.cluster_name
    GithubRepo = "terraform-aws-eks"
    GithubOrg  = "terraform-aws-modules"
  }
}


################################################################################
# Additional security groups for workers
################################################################################

/* resource "aws_security_group" "worker_group_mgmt_one" {
  name_prefix = "worker_group_mgmt_one"
  vpc_id      = module.vpc.vpc_id

  ingress {
    from_port = 22
    to_port   = 22
    protocol  = "tcp"

    cidr_blocks = [
      "10.0.0.0/8",
    ]
  }
} */
/* 
resource "aws_security_group" "all_worker_mgmt" {
  name_prefix = "all_worker_management"
  vpc_id      = module.vpc.vpc_id

  ingress {
    from_port = 22
    to_port   = 22
    protocol  = "tcp"

    cidr_blocks = [
      "10.0.0.0/8",
      "172.16.0.0/12",
      "192.168.0.0/16",
    ]
  }
} */


################################################################################
# Kubernetes provider configuration
################################################################################

/* data "aws_caller_identity" "current" {} */

data "aws_eks_cluster" "cluster" {
  name = module.eks.cluster_id
}

data "aws_eks_cluster_auth" "cluster" {
  name = module.eks.cluster_id
}

provider "kubernetes" {
  host                   = data.aws_eks_cluster.cluster.endpoint
  cluster_ca_certificate = base64decode(data.aws_eks_cluster.cluster.certificate_authority[0].data)
  token                  = data.aws_eks_cluster_auth.cluster.token
}