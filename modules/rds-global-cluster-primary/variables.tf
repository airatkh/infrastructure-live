variable "region" {
  description = "Cluster Region"
  type        = string
}


variable "global_cluster_identifier_name" {
  description = "Global Cluster identifier/name"
  type        = string
}


/* 
  VPC Settings
 */
variable "vpc_id" {
  description = "The ID of the VPC"
  type        = string
}

variable "database_subnet_group_name" {
  description = "ID of database subnet group"
  type        = string
}

variable "private_subnets_cidr_blocks" {
  description = "List of cidr_blocks of private subnets"
  type        = list(string)
}

variable "is_primary_cluster" {
  description = "Determines whether cluster is primary cluster with writer instance (set to `false` for global cluster and replica clusters)"
  type        = bool
  default     = true
}

variable "source_region" {
  description = "The source region for an encrypted replica DB cluster"
  type        = string
  default     = null
}

variable "aurora_tags" {
  description = "Tags to set for all resources"
  type        = map(string)
}