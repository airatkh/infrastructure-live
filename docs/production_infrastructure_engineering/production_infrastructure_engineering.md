# Production Infrastructure Engineering

## Operating a multi-regional application using Terraform Tool

### Infrastructure example
### Operating a multi-regional stateless application using Amazon EKS:

![](https://d2908q01vomqb2.cloudfront.net/fe2ef495a1152561572949784c16bf23abb28057/2020/12/05/image-10.jpg)


### Terraform Support Dev & Stage & Prod: [Example Terraform ](http://joxi.ru/DmBnvRZFJKy4Wm)

# List of Tasks:

## Deployment & automation tools:

- Infrastructure as a code. Develop Infrastructure as source code. The Main tool is Terraform. Leaning & Testing 
- Configure Continuous Integration / Continuous Delivery (CI/CD) with support two Kubernetes Cluster (BitBucker + Helm) 
- Service Mesh: Install & Configure. [The Istio service mesh](https://istio.io/latest/)  
- Deploy [image-resizer-imagehandlerfunction](https://bitbucket.org/harvestonlinestrategies/image-resizer-imagehandlerfunction/src/master/) serverless apps using Lambda and API Gateway 

## Autoscaling:

- [Applying Cluster Scaling with Cluster Autoscaler](https://aws-eks-web-application.workshop.aws/en/100-scaling/200-cluster-scaling.html) 
- [Kubernetes Horizontal Pod Autoscale](https://kubernetes.io/docs/tasks/run-application/horizontal-pod-autoscale/)

## Security:

- Create a new AWS account for the Production environment. AWS Identity and Access Management [Best Practices for Organizational Units with AWS Organizations](https://aws.amazon.com/blogs/mt/best-practices-for-organizational-units-with-aws-organizations/) 
- RateLimit Settings - [AWS WAF - Web Application Firewall](https://aws.amazon.com/waf/) 
- Configire [CloudFront](https://aws.amazon.com/cloudfront/) as a [Content Distribution Network (CDN)](https://en.wikipedia.org/wiki/Content_delivery_network) 
- SSL Support for httpS. [Cloud native certificate management cert-manager.io](https://cert-manager.io/) 

## Multi-Region Availability:

- [Creating a Global Datastore Redis sets up cross region replication between primary and secondary cluster.](https://docs.aws.amazon.com/AmazonElastiCache/latest/red-ug/Redis-Global-Datastore.html) 
- [Using Amazon Aurora global databases](https://docs.aws.amazon.com/AmazonRDS/latest/AuroraUserGuide/aurora-global-database.html) 
- Set up load balancing & "Route53" router depends on the location of the user. Distribute load across your apps and Availability Zones.

## Monitoring

- [Creating Amazon Route 53 health checks](https://docs.aws.amazon.com/Route53/latest/DeveloperGuide/dns-failover.html)
- Configure AWS CloudWatch Metrics 
- Set up Slack alerts (SNS + Lambda Python + Slack WebHook)
- Store logs: To prevent log files from taking up too much disk space

## Testing 

- Perform load tests with Jmeter

## Cost optimization

- Create billing alarms 
- Learn to analyze your AWS bill 

## Others

- Configure / Test your backups backup for all of your data stores.

## Useful & Related Links
---
- [Operating a multi-regional stateless application using Amazon EKS](https://aws.amazon.com/blogs/containers/operating-a-multi-regional-stateless-application-using-amazon-eks/)

- [Creating Kubernetes Auto Scaling Groups for Multiple Availability Zones](https://aws.amazon.com/blogs/containers/amazon-eks-cluster-multi-zone-auto-scaling-groups/)

-  [AWS workshop: Disaster Recovery & High Availability](https://disaster-recovery.workshop.aws/en/intro.html)

- [Production Readiness Checklist](https://gruntwork.io/devops-checklist/)

- [Terraform: 5 Lessons Learned From Writing Over 300,000 Lines of Infrastructure Code](https://www.youtube.com/watch?v=RTEgE2lcyk4)

- [How to manage a multi-region, multi-environment infrastructure on AWS using Terraform](https://dev.to/sdileep/manage-a-multi-environment-multi-region-infrastructure-on-aws-using-terraform-1p2p)