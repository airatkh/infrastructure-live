# Kubernetes feature for developers ( like reading logs using kubectl, checking pods, port-forwarding)

**Documentation for Kubernetes features **

  

This Documentation for developers to:

1.  Get real time logs from services
2.  Get service successfully deployed or not

Get Access  from DevOps to your project:

First of all you need to install SDK: [Google SDK](https://cloud.google.com/sdk/docs/install#deb)

  

[kubectl/cheatsheet](https://kubernetes.io/ru/docs/reference/kubectl/cheatsheet/)

  

After you installed gcloud to your computer ( This example for linux, but another OS i think is similar)

You need to open terminal and wrote commands below: ( You can just copy line by line with comments, it’ll be work)

  

Open web-browser and you need to select your @architech.nyc email and give access.

```plain
gcloud auth login 
```

  

Get credentials for kubernetes

```plain
gcloud container clusters get-credentials cluster-name --zone=zone-c 
```

  

To get pods from “dev” stage of IM

```bash
kubectl -n ns-name02 get pods
```

  

```bash
NAME                                                 READY   STATUS    RESTARTS   AGE
pods_name-5d8f946c9-r5wcz            2/2     Running   0          7h20m
pods_name895dd977-vh55w              2/2     Running   0          53m
pods_name587zx                       2/2     Running   0          20h
pods_namev                           1/1     Running   0          29h
pods_name8c-tkml4                    1/1     Running   0          7h20m
pods_nameg6kmc                       1/1     Running   0          7h20m
pods_name5d565568c-82n9w             1/1     Running   0          7h20m
pods_nameab-runner-546fd9c8f-5dfgw   1/1     Running   0          29h
pods_namece-7664479c84-7zrhj         1/1     Running   0          24h
pods_namecfd597f5-fglml              2/2     Running   0          24h
pods_namer-service-954845dbc-dwcgh   1/1     Running   0          24h
pods_name-ngcjb                      1/1     Running   0          25h
pods_namefb6965-5cwq7                2/2     Running   0          130m
```

  

To get pods from “stage” stage of IM

```plain
kubectl -n ns-name get pods
```

  

To get pods from “prod” stage of IM

```plain
kubectl -n ns-name get pods
```

  

Okay, Now you have access to Kubernetes, now we will try to see logs from services: 

_for example we wanna see logs from business-logic_

```plain
kubectl -n ns-name02 get pods | grep pods_name
pods_name-logic-service-85895dd977-vh55w              2/2     Running   0          56m
```

  

_Examples(how to see logs):_

```plain
kubectl -n ns-name02 logs pods-name-service-85895dd977-vh55w pods-name-service
```

  

```plain
kubectl -n ns-name02 logs pods-name-service-85895dd977-vh55w pods-name-service --tail 4
```

  

```plain
kubectl -n ns-name02 logs -f pods-name-service-85895dd977-vh55w pods-name-service
```

  

  

Work with k8s contexts

  

```bash
kubectl config get-contexts
```

  

```plain
kubectl config rename-context gke_gcp-name_zone-c_cluster-name develop-im
```

  

```plain
kubectl config set-context --current --namespace im-dev
```

  

```plain
kubectl config use-context develop-im
```