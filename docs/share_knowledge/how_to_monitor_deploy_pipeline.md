# How to monitor deploy/pipeline

  

How to monitor deploy/pipeline?
===============================

  

_pre requirements:_

  

Please install AWS CLI for your Mac OS

[https://docs.aws.amazon.com/cli/latest/userguide/getting-started-install.html](https://docs.aws.amazon.com/cli/latest/userguide/getting-started-install.html)

  

We will follow together by that link. I will share the screen.

Create security\_credentials AWS EKS Onboarding Link to Doc

You need to paste command bellow in terminal | cmd to authenticate in AWS:

  

```plain
aws configure
```

  

```plain
aws eks --region region-1 update-kubeconfig --name clustername
```

  

Basic commands

kubectl get pods --namespace prod

  

```plain
kubectl get --namespace prod hpa,nodes,pods,cronjob,jobs
kubectl get -n prod hpa,nodes,pods,cronjob,jobs
```

  

```plain
watch kubectl get --namespace prod hpa,nodes,pods,cronjob,jobs
```

  

How to see logs:

  

```plain
kubectl logs -n prod  -f deployments.apps/leafy-things
```

  

```plain
env | grep SHELL
```

  

Useful Link: [kubectl Cheat Sheet](https://kubernetes.io/docs/reference/kubectl/cheatsheet/)

  

`clustername-web git:(stage) eksctl get iamidentitymapping --cluster clustername --profile k8s`

  

```plain
2021-12-10 11:21:31 [ℹ]  eksctl version 0.58.0
2021-12-10 11:21:31 [ℹ]  using region region-1
ARN									USERNAME				GROUPS
arn:aws:iam::111111111111111:user/akhisamov				akhisamov				system:masters
```

  

`clustername-web git:(stage) eksctl create iamidentitymapping --cluster clustername --arn arn:aws:iam::111111111111111:user/username --group system:masters --username username`

  

```plain
2021-12-10 11:32:08 [ℹ]  eksctl version 0.58.0
2021-12-10 11:32:08 [ℹ]  using region region-1
2021-12-10 11:32:10 [ℹ]  adding identity "arn:aws:iam::111111111111111:user/username" to auth ConfigMap
```