# Context: Configure Access to Multiple Clusters

  

Configure Access to Multiple Clusters
=====================================

  

1\. Listing existing `contexts` / current active `context`
----------------------------------------------------------

  

```plain
kubectl config get-contexts
```

  

Output example:

  

```plain
CURRENT   NAME                                                 CLUSTER                                                 AUTHINFO                                                NAMESPACE
          xxx-dev-gcp_proj_name_us-central1                    gke_gcp_proj_name-155809_us-central1-c_arch-cluster        gke_gcp_proj_name-155809_us-central1-c_arch-cluster        xxx-dev
*         xxx-prod-new                                         gke_gcp_proj_name-155809_us-central1-c_arch-cluster-prod   gke_gcp_proj_name-155809_us-central1-c_arch-cluster-prod   xxx-prod
          xxx-stage-gcp_proj_name_us-central1                  gke_gcp_proj_name-155809_us-central1-c_arch-cluster        gke_gcp_proj_name-155809_us-central1-c_arch-cluster        xxx-stage


```

  

2\. Create `contexts`
---------------------

  

**TIP**: If your output empty make two requests

  

`Create` access to `dev/stage` enviroments. Cluster `arch-cluster`

  

```plain
gcloud container clusters get-credentials arch-cluster -z us-central1-c
```

  

`Create` access to `production` enviroments. Cluster `arch-cluster-prod`

  

```plain
gcloud container clusters get-credentials arch-cluster-prod -z us-central1-c
```

  

3\. Switch `context` (access) to production/develop cluster.
------------------------------------------------------------

  

**TIP**: Use your content name from your output

  

`Change/Switch` access to `production` enviroments.

  

```plain
kubectl config use-context u4u-production-example-name
```

  

`Change/Switch` access to `develop/stage` enviroments.

  

```plain
kubectl config use-context xxx-dev-example-name
```

  

4\. Rename `context`
--------------------

  

**TIP**: Given name should make sense for you.

  

```plain
kubectl config rename-context u4u-production-project_europe-west1-c_cluster  xxx-prod
```

  

5\. Detete `context`.
---------------------

  

**TIP**: Delete unused /messy `context`. You can easily create new `content`.

  

```plain
kubectl config delete-context gke_xx-project_europe-west1-c_cluster-unused-name-example
```

  

6\. Set default `namespace` for `current` active `context`
----------------------------------------------------------

  

**TIP**: you will not need to write namespace every time, just set the default `namespace` for every `context` one time.

  

```plain
kubectl config set-context --current --namespace xxx-prod
```

  

```plain
kubectl config set-context --current --namespace xxx-dev
```

  

```plain
kubectl config set-context --current --namespace xxx-stage
```