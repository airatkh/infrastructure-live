# GitLab upgrade "todo" list.

  

Main Documentation
==================

  

[https://docs.gitlab.com/ee/raketasks/backup\_restore.html#back-up-gitlab](https://docs.gitlab.com/ee/raketasks/backup_restore.html#back-up-gitlab)

[https://about.gitlab.com/install/?version=ce](https://about.gitlab.com/install/?version=ce)

  

Commands BackUp
===============

  

gitlab-backup create
====================

  

```plain
Set Domain Name:
gitlab.hostname.nyc.	A	300	 XX.XX.XX.XXX
gitlab2.hostname.nyc.	A	300	 YY.YY.YY.YYY
```

  

```plain
curl -s https://packages.gitlab.com/install/repositories/gitlab/gitlab-ce/script.deb.sh | sudo bash
```

  

```plain
export EXTERNAL_URL="http://gitlab2.hostname.nyc/" 
```

```plain
sudo apt-get install gitlab-ce=12.10.6-ce.0
```

  

Repositiry for CE version
=========================

  

[https://packages.gitlab.com/gitlab/gitlab-ce](https://packages.gitlab.com/gitlab/gitlab-ce)

[https://packages.gitlab.com/app/gitlab/gitlab-ce/search?q=12.10.6](https://packages.gitlab.com/app/gitlab/gitlab-ce/search?q=12.10.6)

  

Ubuntu version
==============

  

```plain
root@gitlab2:/etc/apt# lsb_release -a
No LSB modules are available.
Distributor ID:	Ubuntu
Description:	Ubuntu 20.04.1 LTS
Release:	20.04
Codename:	focal
```

  

Installing Google Cloud SDK
===========================

  

[https://cloud.google.com/sdk/docs/install](https://cloud.google.com/sdk/docs/install)

  

```plain
gcloud init
gcloud config set compute/zone zone_region-c
gcloud config set compute/region zone_region
gcloud config list

#Other usefull commands
  gcloud config configurations list
  gcloud config configurations activate
  gcloud config configurations create
  gcloud config configurations delete
  gcloud config configurations describe
```

  

Add a new Disk & mount to a new GitLab2
=======================================

  

300 Gb

  

Copy backup to Cloud
====================

  

```plain
gsutil -m cp -R gitlab_backups gs://gitlab-xxxxx-cache/buckup_gitlab/
gsutil cp -R  /etc/gitlab gs://gitlab-xxxxx-cache/gitlab_config
```

  

Copy From Cloud to a new Gitlab2
================================

  

```plain
# Restore configs
gsutil cp -R  gs://gitlab-xxxxx-cache/gitlab_config/gitlab .
gitlab-ctl reconfigure
gitlab-ctl start
```

  

Restore backup to default local folder
======================================

  

```plain
gsutil cp  gs://gitlab-xxxxx-cache/buckup_gitlab/gitlab_backups/1619768927_2021_04_30_12.10.6_gitlab_backup.tar  /var/opt/gitlab/backups

chown git.git /var/opt/gitlab/backups/1619768927_2021_04_30_12.10.6_gitlab_backup.tar
```

  

```plain
#Stop the processes that are connected to the database. Leave the rest of GitLab running:
sudo gitlab-ctl stop unicorn
sudo gitlab-ctl stop puma
sudo gitlab-ctl stop sidekiq
# Verify
sudo gitlab-ctl status

# This command will overwrite the contents of your GitLab database!
sudo gitlab-backup restore BACKUP=1619768927_2021_04_30_12.10.6
```

  

Restore for Omnibus GitLab installations
========================================

  

[https://docs.gitlab.com/ee/raketasks/backup\_restore.html#restore-for-omnibus-gitlab-installations](https://docs.gitlab.com/ee/raketasks/backup_restore.html#restore-for-omnibus-gitlab-installations)

  

Mail not working. Debug.
========================

  

[https://docs.gitlab.com/ee/administration/troubleshooting/debug.html#mail-not-working](https://docs.gitlab.com/ee/administration/troubleshooting/debug.html#mail-not-working)

  

```plain
sudo gitlab-rails console
```

  

[https://docs.gitlab.com/omnibus/settings/smtp.html](https://docs.gitlab.com/omnibus/settings/smtp.html)

  

TODO Upgrade packaged PostgreSQL server
=======================================

  

```plain
sudo apt-get install gitlab-ce=12.10.14-ce.0
sudo apt-get install gitlab-ce=13.0.0-ce.0
sudo apt-get install gitlab-ce=13.1.2-ce.0
sudo apt-get install gitlab-ce=13.2.10-ce.0
sudo apt-get install gitlab-ce=13.3.9-ce.0
sudo apt-get install gitlab-ce=13.4.7-ce.0
sudo apt-get install gitlab-ce=13.5.7-ce.0
sudo apt-get install gitlab-ce=13.6.7-ce.0
sudo apt-get install gitlab-ce=13.7.9-ce.0
sudo apt-get install gitlab-ce=13.8.8-ce.0
sudo apt-get install gitlab-ce=13.9.7-ce.0
```

  

GitLab now ships with a newer version of PostgreSQL (12.5) and you are recommended to upgrade to it.
----------------------------------------------------------------------------------------------------

  

To upgrade, please see:

  

[https://docs.gitlab.com/omnibus/settings/database.html#upgrade-packaged-postgresql-server](https://docs.gitlab.com/omnibus/settings/database.html#upgrade-packaged-postgresql-server)

  

SendGrid API Key GitLab in DM Airat
-----------------------------------

  

Broadcast message
=================

  

GitLab has been upgraded.

Could you please verify your email. Any question, please contact me, Airat Khisamov