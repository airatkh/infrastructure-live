# Cloud SQL committed use discounts

Cloud SQL committed use discounts
---------------------------------

  

|  | <br>**Instances**<br> | <br>**vCPUs**<br> | **price per vCPU/hour** | <br>**Memory Gb**<br> | **price per GB/hour** |
| ---| ---| ---| ---| ---| --- |
| **1** | dev01 | 1 | 0.0413 | 3.75 | 0.0070 |
| **2** | dev02 | 1 | 0.0413 | 3.75 | 0.0070 |
| **3** | dev03 | 1 | 0.0413 | 3.75 | 0.0070 |
| **4** | dev04 | 1 | 0.0413 | 3.75 | 0.0070 |
| **5** | dev05 | 1 | 0.0413 | 3.75 | 0.0070 |

  

From the [pricing table](https://cloud.google.com/sql/pricing#sql-cpu-mem-pricing), we can calculate the total hourly commitment cost per region:

  

### lowa (us-central1)

  

### 5 instances x **1 vCPUs x** $0.0413 per vCPU/hour = ~$0.2065 / hour

### 5 instances x **3.**75Gb RAM **x** $0.0070 per GB/hour = ~$0,13125 / hour

  

### For a total of $0,33775 =~ ($0.34) per hour in committed use hourly pricing

  

### Calculating the monthly cost (720 hours in a month):

  

### Iowa

### On-demand pricing = ($0.34/hour \* 720 hours) = $244,8 / month

  

### 1-year committed use discounts discount. After 25%,

### Totals a savings of $61.2 / month x 12 month x 1 year = $723,4

  

### 3-years committed use discounts discount. After 52%,

### Totals a savings of $127.30 / month x 12 month x 3 years = $4582,8

### Information for Purchase a committed use discount:

### Commitment name:

main-cloud-sql-commitment

Region:

us-central1

Hourly on-demand commitment:

0.34