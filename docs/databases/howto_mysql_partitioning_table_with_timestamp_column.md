# HowTo MySql Partitioning table with timestamp column.

  

Official doc link : [Chapter 22 Partitioning](about:blank)

  

"HowTo" based on article [MySql table partition (based on datetime)](https://programmer.group/mysql-table-partition-based-on-datetime.html)

  

1 Has been created a new table `tbl_tablname_biz02.`

this table will be use instead of `tbl_tablname_biz`

  

  

```sql
CREATE TABLE `tbl_tablname_biz02` (
  `anaID` varchar(40) COLLATE utf8mb4_unicode_ci NOT NULL,
  `anaBusinessID` varchar(40) COLLATE utf8mb4_unicode_ci NOT NULL,
  `anaUserAction` varchar(40) COLLATE utf8mb4_unicode_ci NOT NULL,
  `anaUserID` varchar(40) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `anaUserIP` varchar(40) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`anaID`, `created_at`),
  KEY `idx_tbl_tablname_biz_anaBusinessID` (`anaBusinessID`),
  KEY `idx_tbl_tablname_biz_created_at` (`created_at`),
  KEY `idx_tbl_tablname_biz_anaUserAction` (`anaUserAction`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci 
PARTITION BY RANGE ( UNIX_TIMESTAMP (created_at)) (
	PARTITION p202101 VALUES LESS THAN (UNIX_TIMESTAMP('2021-02-01 00:00:00')) ENGINE = InnoDB,
 	PARTITION p202102 VALUES LESS THAN (UNIX_TIMESTAMP('2021-03-01 00:00:00')) ENGINE = InnoDB,
 	PARTITION p202103 VALUES LESS THAN (UNIX_TIMESTAMP('2021-04-01 00:00:00')) ENGINE = InnoDB,
 	PARTITION p202104 VALUES LESS THAN (UNIX_TIMESTAMP('2021-05-01 00:00:00')) ENGINE = InnoDB,
 	PARTITION p202105 VALUES LESS THAN (UNIX_TIMESTAMP('2021-06-01 00:00:00')) ENGINE = InnoDB,
 	PARTITION p202106 VALUES LESS THAN (UNIX_TIMESTAMP('2021-07-01 00:00:00')) ENGINE = InnoDB,
 	PARTITION p202107 VALUES LESS THAN (UNIX_TIMESTAMP('2021-08-01 00:00:00')) ENGINE = InnoDB,
 	PARTITION p202108 VALUES LESS THAN (UNIX_TIMESTAMP('2021-09-01 00:00:00')) ENGINE = InnoDB,
 	PARTITION p202109 VALUES LESS THAN (UNIX_TIMESTAMP('2021-10-01 00:00:00')) ENGINE = InnoDB,
 	PARTITION p202110 VALUES LESS THAN (UNIX_TIMESTAMP('2021-11-01 00:00:00')) ENGINE = InnoDB,
 	PARTITION p202111 VALUES LESS THAN (UNIX_TIMESTAMP('2021-12-01 00:00:00')) ENGINE = InnoDB,
 	PARTITION p202112 VALUES LESS THAN (UNIX_TIMESTAMP('2022-01-01 00:00:00')) ENGINE = InnoDB,
    
	PARTITION p202201 VALUES LESS THAN (UNIX_TIMESTAMP('2022-02-01 00:00:00')) ENGINE = InnoDB,
 	PARTITION p202202 VALUES LESS THAN (UNIX_TIMESTAMP('2022-03-01 00:00:00')) ENGINE = InnoDB,
 	PARTITION p202203 VALUES LESS THAN (UNIX_TIMESTAMP('2022-04-01 00:00:00')) ENGINE = InnoDB,
 	PARTITION p202204 VALUES LESS THAN (UNIX_TIMESTAMP('2022-05-01 00:00:00')) ENGINE = InnoDB,
 	PARTITION p202205 VALUES LESS THAN (UNIX_TIMESTAMP('2022-06-01 00:00:00')) ENGINE = InnoDB,
 	PARTITION p202206 VALUES LESS THAN (UNIX_TIMESTAMP('2022-07-01 00:00:00')) ENGINE = InnoDB,
 	PARTITION p202207 VALUES LESS THAN (UNIX_TIMESTAMP('2022-08-01 00:00:00')) ENGINE = InnoDB,
 	PARTITION p202208 VALUES LESS THAN (UNIX_TIMESTAMP('2022-09-01 00:00:00')) ENGINE = InnoDB,
 	PARTITION p202209 VALUES LESS THAN (UNIX_TIMESTAMP('2022-10-01 00:00:00')) ENGINE = InnoDB,
 	PARTITION p202210 VALUES LESS THAN (UNIX_TIMESTAMP('2022-11-01 00:00:00')) ENGINE = InnoDB,
 	PARTITION p202211 VALUES LESS THAN (UNIX_TIMESTAMP('2022-12-01 00:00:00')) ENGINE = InnoDB,
 	PARTITION p202212 VALUES LESS THAN (UNIX_TIMESTAMP('2023-01-01 00:00:00')) ENGINE = InnoDB,
    
	PARTITION p202301 VALUES LESS THAN (UNIX_TIMESTAMP('2023-02-01 00:00:00')) ENGINE = InnoDB,
 	PARTITION p202302 VALUES LESS THAN (UNIX_TIMESTAMP('2023-03-01 00:00:00')) ENGINE = InnoDB,
 	PARTITION p202303 VALUES LESS THAN (UNIX_TIMESTAMP('2023-04-01 00:00:00')) ENGINE = InnoDB,
 	PARTITION p202304 VALUES LESS THAN (UNIX_TIMESTAMP('2023-05-01 00:00:00')) ENGINE = InnoDB,
 	PARTITION p202305 VALUES LESS THAN (UNIX_TIMESTAMP('2023-06-01 00:00:00')) ENGINE = InnoDB,
 	PARTITION p202306 VALUES LESS THAN (UNIX_TIMESTAMP('2023-07-01 00:00:00')) ENGINE = InnoDB,
 	PARTITION p202307 VALUES LESS THAN (UNIX_TIMESTAMP('2023-08-01 00:00:00')) ENGINE = InnoDB,
 	PARTITION p202308 VALUES LESS THAN (UNIX_TIMESTAMP('2023-09-01 00:00:00')) ENGINE = InnoDB,
 	PARTITION p202309 VALUES LESS THAN (UNIX_TIMESTAMP('2023-10-01 00:00:00')) ENGINE = InnoDB,
 	PARTITION p202310 VALUES LESS THAN (UNIX_TIMESTAMP('2023-11-01 00:00:00')) ENGINE = InnoDB,
 	PARTITION p202311 VALUES LESS THAN (UNIX_TIMESTAMP('2023-12-01 00:00:00')) ENGINE = InnoDB,
 	PARTITION p202312 VALUES LESS THAN (UNIX_TIMESTAMP('2024-01-01 00:00:00')) ENGINE = InnoDB,

 	PARTITION p_future VALUES LESS THAN MAXVALUE ENGINE = InnoDB);
```

  

  

2\. Created backup table `tbl_tablname_biz`

3\. Restored backup table `tbl_tablname_biz` to a new table `tbl_tablname_biz02`

4\. Renamed old table `tbl_tablname_biz` without partitions to `tbl_tablname_biz_orifg`

5\. Renamed a new table `bl_tablname_biz02` with partitions settings to `tbl_tablname_biz`

  

  

Useful SQL requests to test/check tablname SQL queries

  

```sql
select count(*) as aggregate 
from `tbl_business` 
inner join (
    select `anaBusinessID`, count(*) as viewCount 
    from `tbl_tablname_biz` 
    where 
        `anaUserAction` in ('view', 'top') and
        `created_at` >= '2021-09-01 00:00:00' and
        `created_at` < '2021-10-01 00:00:00'
    group by `anaBusinessID`) as `a` 
    on `bizBusinessID` = `a`.`anaBusinessID` 
    left join (
        select `anaBusinessID`, count(*) as engageCount 
        from `tbl_tablname_biz` 
        where `anaUserAction` not in ('view', 'appview') and
          `created_at` >= '2021-09-01 00:00:00' and
          `created_at` < '2021-10-01 00:00:00' 
        group by `anaBusinessID`) as `b`
        on `a`.`anaBusinessID` = `b`.`anaBusinessID` 
        left join (
            select `anaBusinessID`, count(*) as appViewACount 
            from `tbl_tablname_biz` 
            where `anaUserAction` = 'appview' and
            `created_at` >= '2021-09-01 00:00:00' and
            `created_at` < '2021-10-01 00:00:00' 
            group by `anaBusinessID`) as `c` 
            on `bizBusinessID` = `c`.`anaBusinessID` 
            left join (
                select `anaBusinessID`, count(*) as topViewCount
                from `tbl_tablname_biz` 
                where `anaUserAction` = 'top' and
                `created_at` >= '2021-09-01 00:00:00' and
                `created_at` < '2021-10-01 00:00:00' 
                group by `anaBusinessID`) as `d` 
                on `bizBusinessID` = `d`.`anaBusinessID` 
                left join (
                    select `anaBusinessID`, count(*) as dealsViewCount
                    from `tbl_tablname_deals` where `anaUserAction` = 'view'
                    and `created_at` >= '2021-09-01 00:00:00'
                    and `created_at` < '2021-10-01 00:00:00'
                    group by `anaBusinessID`) as `e` 
                    on `bizBusinessID` = `e`.`anaBusinessID` 
                    left join (
                        select `anaBusinessID`, count(*) as dealengageCount
                        from `tbl_tablname_deals`
                        where `anaUserAction` not in ('view', 'appview') and
                        `created_at` >= '2021-09-01 00:00:00' and
                        `created_at` < '2021-10-01 00:00:00'
                        group by `anaBusinessID`) as `f` on `bizBusinessID` = `f`.`anaBusinessID`
```

  

```sql
select * from `tbl_business`
 inner join (
     select `anaBusinessID`, count(*) as viewCount
     from `tbl_tablname_biz`
     where `anaUserAction` in ('view', 'top') and
     `created_at` >= '2021-09-01 00:00:00' and
     `created_at` < '2021-10-01 00:00:00'
     group by `anaBusinessID`) as `a` on `bizBusinessID` = `a`.`anaBusinessID`
     left join (
         select `anaBusinessID`, count(*) as engageCount
         from `tbl_tablname_biz`
         where `anaUserAction` not in ('view', 'appview') and
         `created_at` >= '2021-09-01 00:00:00' and
         `created_at` < '2021-10-01 00:00:00'
         group by `anaBusinessID`) 
         as `b` on `a`.`anaBusinessID` = `b`.`anaBusinessID`
         left join (
             select `anaBusinessID`, count(*) as appViewACount
             from `tbl_tablname_biz`
             where `anaUserAction` = 'appview' and
             `created_at` >= '2021-09-01 00:00:00' and
             `created_at` < '2021-10-01 00:00:00' group by `anaBusinessID`)
             as `c` on `bizBusinessID` = `c`.`anaBusinessID`
             left join (
                 select `anaBusinessID`, count(*) as topViewCount
                 from `tbl_tablname_biz` 
                 where `anaUserAction` = 'top' and
                 `created_at` >= '2021-09-01 00:00:00' and
                 `created_at` < '2021-10-01 00:00:00'
                  group by `anaBusinessID`)
                  as `d` on `bizBusinessID` = `d`.`anaBusinessID`
                  left join (
                      select `anaBusinessID`, count(*) as dealsViewCount
                      from `tbl_tablname_deals` 
                      where `anaUserAction` = 'view' and
                      `created_at` >= '2021-09-01 00:00:00' and
                      `created_at` < '2021-10-01 00:00:00' 
                    group by `anaBusinessID`) 
                    as `e` on `bizBusinessID` = `e`.`anaBusinessID` 
                    left join (
                        select `anaBusinessID`, count(*) as dealengageCount
                        from `tbl_tablname_deals` 
                        where `anaUserAction` not in ('view', 'appview') and
                        `created_at` >= '2021-09-01 00:00:00' and
                        `created_at` < '2021-10-01 00:00:00'
                         group by `anaBusinessID`) as `f`
                         on `bizBusinessID` = `f`.`anaBusinessID` order by `bizName` asc limit 10 offset 0
```

  

```sql
select 
    d.bizRegionID,sum(viewCount) as views , 
    sum(appViewCount) as appViewCount,
    sum(engageCount) as engagements 
    from `tbl_business`
    inner join 
    (select `anaBusinessID`, count(*) as viewCount 
    from `tbl_tablname_biz` 
    where `anaUserAction` = 'view' and
    `created_at` >= '2021-09-01 00:00:00' and
    `created_at` < '2021-10-01 00:00:00' 
    group by `anaBusinessID`) as `a` on `bizBusinessID` = `a`.`anaBusinessID`
    left join (
        select `anaBusinessID`, count(*) as engageCount
        from `tbl_tablname_biz`
        where `anaUserAction` not in ('view', 'appview') and
        `created_at` >= '2021-09-01 00:00:00' and
        `created_at` < '2021-10-01 00:00:00' 
        group by `anaBusinessID`) as `b` on `a`.`anaBusinessID` = `b`.`anaBusinessID` 
        left join 
        (
            select `anaBusinessID`, count(*) as appViewCount
            from `tbl_tablname_biz` 
            where `anaUserAction` = 'appview' and
            `created_at` >= '2021-09-01 00:00:00' and
            `created_at` < '2021-10-01 00:00:00' 
            group by `anaBusinessID`) as `c` on `bizBusinessID` = `c`.`anaBusinessID`
             right join (
                 select `bizBusinessID`, `bizRegionID` 
                 from `tbl_business`) as `d` on `a`.`anaBusinessID` = `d`.`bizBusinessID`
                 group by `d`.`bizRegionID` order by `views` desc, `views` desc
```

  

Rename table name in backup files command

  

```bash
sed -i -e 's/tbl_tablname_biz/tbl_tablname_biz02/' leafythings_tablname_prod_tbl_tablname_biz.sql
sed -i -e 's/tbl_tablname_biz/tbl_tablname_biz02/' leafythings_tablname_prod_tbl_business.sql
sed -i -e 's/tbl_tablname_biz/tbl_tablname_biz02/' leafythings_tablname_prod_migrations.sql
sed -i -e 's/tbl_tablname_biz/tbl_tablname_biz02/' leafythings_tablname_prod_tbl_tablname_deals.sql
sed -i -e 's/tbl_tablname_biz/tbl_tablname_biz02/' leafythings_tablname_prod_tbl_deals.sql
```